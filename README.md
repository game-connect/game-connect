# Game Connect

A NativeScript Project to connect Mobile devices by Wifi or Bluetooth.

## Dependencies

- p2pkit API : `http://p2pkit.io/`
- Our homemade NativeScript plugin p2pkit : `https://gitlab.com/game-connect/nativescript-plugin-p2pkit`